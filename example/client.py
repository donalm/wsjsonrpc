#!/usr/bin/env python

import os
import sys
import time

from twisted import logger
from twisted.internet import defer
from twisted.internet import task

sys.path.append(os.path.dirname(os.path.dirname(os.path.realpath(__file__))))

from wsjsonrpc import factory

logger = logger.Logger(
    observer=logger.textFileLogObserver(sys.stdout), #, recordSeparator=""),
    namespace=''
)


def echo(protocol, value):
    logger.debug("peer initiated request: 'echo': {}".format(value))
    return "echo result: {}".format(value)

def show_result(value):
    logger.debug("show_result: {}".format(value))
    return value

def log_error(f):
    logger.error("error", traceback=f.getBriefTraceback())


@defer.inlineCallbacks
def main(reactor):

    result = None
    protocol = yield factory.get_client(hostname="localhost", port=8095, path=u"wsjsonrpc")

    """
    Grant permission for peers to execute our 'echo' method.
    """
    protocol.factory.registerMethod("echo", echo)

    """
    Call the 'sum' method on our peer and log the result.
    """
    for i in range(5):
        start = time.time()
        result = yield protocol.request("sum", [1024, 8192])
        logger.debug(str(result))
        duration = time.time() - start
        logger.debug("roundtrip time for 'sum' method is: {} seconds".format(duration))

    result = yield protocol.request("sum", [1024, 8192])
    logger.debug(str(result))

    result = yield protocol.request("echo", ["What's the frequency Kenneth?"])
    logger.debug(str(result))

    """
    JSONRPC BATCH REQUESTS:

    We can create a RequestBatcher by opening a context, and calling the
    request or notify methods on the context. When we exit the context,
    the batch request will be submitted.
    """
    deferred_list = []
    with protocol.batchContext() as batch:
        deferred_list.extend([
            batch.request("echo", ["Echo 000"]),
            batch.request("echo", ["Echo 001"]),
            batch.request("echo", ["Echo 002"]),
            batch.request("echo", ["Echo 003"]),
        ])

        """
        The 'notify' method will not elicit a result from the server, so it
        doesn't return a deferred; it just returns None.
        """
        batch.notify("echo", ["This will succeed"])         # <-- Good: the argument must be a dict or list

        deferred_list.extend([
            batch.request("echo", ["Echo 004"]),
            batch.request("sum", [1024, 4096]),
        ])

        """
        Add callbacks to the remote calls individually.
        """
        for df in deferred_list:
            df.addCallback(show_result)
            df.addErrback(log_error)

    results = yield defer.gatherResults(deferred_list)
    logger.debug(str(results))
    yield task.deferLater(reactor, 4, lambda:result)

task.react(main)
